// v1 requirements
// it should have a place to store todos
// it should have a way to display todos
// it should have a way to add new todos
// it should have a way to change a todo
// it should have a way to delete a todo

// v2 requirements
// it should have a function to display todos
// it should have a function to add new todos
// it should have a function to change a todoList
// it should have a function to delete a todo

// v3 requirements
// it should store a todo array in an object
// it should have a method to display todos
// it should have a method to add a todo
// it should have a method to change a todo
// it should have a method to delete a todo

// v4 requirements
// addTodo method should add objects
// changeTodo method should change the todoText property
// create a new method, toggleCompleted

var todoList = {

  // place to store todos (array)
  todos: [],

  // v3: method to display todos
  displayTodos: function () {
    // loop through todos array
      // log each todo to the console
    for (var i = 0; i < this.todos.length; i++) {
      console.log(this.todos[i]);
    }
  },

  // v3: method to add todos
  // v4: an object will be pushed to the todos array
  addTodo: function(todoText) {
    // v3: use the array push method to add a todo to the end of the todo array
    // v4: an object with two properties, todoText and completed, will be pushed to the todo array
    this.todos.push({todoText: todoText, completed: false});
    // display the todos to the console
    this.displayTodos();
  },

  // v3: method to change a todo
  // v4: method will change the todoText property
  changeTodo: function(position, todoText) {
    // v3: access the todo at the provided index and change the text
    // v4: access the todo object at the provided index and change the todoText property
    this.todos[position].todoText = todoText;
    // display the todos to the console
    this.displayTodos();
  },

  // v3: method to delete a todo
  deleteTodo: function(position) {
    // use the array splice method to delete a single todo from the array position
    this.todos.splice(position, 1);
    // display the todos
    this.displayTodos();
  },

  // v4: method to toggle complete/incomplete todos
  toggleCompleted: function(position) {
    // create variable for the todo to toggle
    var todo = this.todos[position];
    // use bang operator on the completed property to flip it to true/false
    todo.completed = !todo.completed;
    // display the todos
    this.displayTodos();
  }
};
